const { Command } = require('discord-akairo');
const akairoVersion = require('discord-akairo').version;
const { MessageEmbed, version } = require('discord.js');

class StatsCommand extends Command {
	constructor() {
		super('stats', {
			aliases: ['stats'],
			category: 'utility',
			description: {
				content: 'Show some stats about the bot',
				usage: '',
				examples: ['']
			}
		});
	}

	async exec(message) {
		var uptime = process.uptime();
		const date = new Date(uptime*1000);
		const days = date.getUTCDate() - 1,
			hours = date.getUTCHours(),
			minutes = date.getUTCMinutes(),
			seconds = date.getUTCSeconds();
	
			
		let segments = [];
	
		// Format the uptime string. 	
		if (days > 0) segments.push(days + ' day' + ((days == 1) ? '' : 's'));
		if (hours > 0) segments.push(hours + ' hour' + ((hours == 1) ? '' : 's'));
		if (minutes > 0) segments.push(minutes + ' minute' + ((minutes == 1) ? '' : 's'));
		if (seconds > 0) segments.push(seconds + ' second' + ((seconds == 1) ? '' : 's'));
		const dateString = segments.join(', ');

		const used = process.memoryUsage().heapUsed / 1024 / 1024;

		const statsEmbed = new MessageEmbed()
			.setColor('#0099ff')
			.setTitle('Bot stats')
			.setAuthor('Haha yes')
			.addField('Servers', this.client.guilds.size, true)
			.addField('Channels', this.client.channels.size, true)
			.addField('Users', this.client.users.size, true)
			.addField('Uptime', dateString, true)
			.addField('Ram usage', `${Math.round(used * 100) / 100} MB`, true)
			.addField('Nodejs version', process.version, true)
			.addField('Discord.js version', version, true)
			.addField('Discord-Akairo version', akairoVersion, true)
			.setTimestamp();
			
		return message.channel.send(statsEmbed);
	}
}

module.exports = StatsCommand;